import { doesArrayExist } from '@utils/utils';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { connect } from 'react-redux';


const INFO_ICON = require(`@resources/icons/info_icon.svg`);

const KnowMoreModal = ({knowMoreMessages}) => {
    const {t} = useTranslation('main');
    let messages;
    
    if(doesArrayExist(knowMoreMessages)) { 
        messages = knowMoreMessages[0].messages.map(msgs => { 
            return <div>
                    <img src={INFO_ICON} className="mr-2" alt="host"/>
                    <span>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco</span>
                </div>
        })
    }
   

    const closeModal = e => { 
        const element = document.getElementById("mmodal");
        element.classList.remove("active");
    }

    return (
            <div className={`message-modal ${doesArrayExist(knowMoreMessages)? 'active': ''}`} id="mmodal">
                <h2 className="title text-center ">{doesArrayExist(knowMoreMessages)? knowMoreMessages[0].title : '-'}</h2>
                <div className="description my-3">
                    {messages}
                </div>
                <span className="text-center w-100 d-block close-btn" onClick={e => closeModal(e)}>{t("close")}</span>
            </div>
    )
}

const mapStateToProps = (state) => ({
    knowMoreMessages : state.admin.knowMoreMessages
})


export default connect(mapStateToProps, {})(KnowMoreModal);
