import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Card, CardBody, Col, Row } from 'reactstrap'
import { useTranslation } from 'react-i18next'
import { calculateNumberOfPages, doesArrayExist, searchObjectInArrayByKeyVal } from '@utils/utils'
import { MAIN_PANEL_VIEW_VISIT_MODAL, NUMBER_USER_PER_PAGE } from '@utils/constants'
import { v4 } from 'uuid';
import { open_modal } from '@actions/admin'
import Badge from 'reactstrap/lib/Badge'
import TableNavigation from '@components/MainPage/Shared/TableNavigation'

const ARROW_RIGHT_ICON = require(`@resources/icons/arrow_right.svg`);
const ARROW_LEFT_ICON = require(`@resources/icons/arrow_left.svg`);


const VisitorsTable = ({visitors, visits, open_modal}) => {
    const { t } = useTranslation('main');
    const [state, setState] = useState({
        page: 1,
    })

    
    const {page} = state;
    const numberOfVisitors = doesArrayExist(visitors)? visitors.length : 1;
    let  totalNumOfPages =  calculateNumberOfPages(numberOfVisitors, NUMBER_USER_PER_PAGE);
    const indicators = Array.from(Array(totalNumOfPages)).map((e, i) => i + 1);
    const next = e => {setState({...state, page: page === totalNumOfPages? totalNumOfPages: page + 1})};
    const prev = e => {setState({...state, page: page === 1? page: page - 1})}
    const setPage = (e, page) => {setState({...state, page })}


    let listOfVisitors = [];
    if (doesArrayExist(visitors)){
        const visitorsOfPage = visitors.filter((req, index) => (index >= ((page-1) * NUMBER_USER_PER_PAGE)) && (index < (page * NUMBER_USER_PER_PAGE)));
        let toolTipName, toolTipDocumentId, visitorName;
        listOfVisitors = visitorsOfPage.map(visitor => { 
            toolTipName = `${visitor.p_fname + ' ' + visitor.p_lname}`;
            toolTipDocumentId = `${visitor.p_nid}`;
            visitorName = visitor.p_fname + ' ' + visitor.p_lname;
            return (
                <tr key={v4()} className={`tr ${visitor.int_id_obs !== null?  "rej":""} py-1"`} onClick={e => onSelectUser(e, visitor)}>
                    <td className="pl-4 text-truncate" data-toggle="tooltip" data-placement="top" title={toolTipName} >{visitorName}</td>
                    <td className="text-truncate" data-toggle="tooltip" data-placement="top" title={toolTipDocumentId}>{visitor.p_nid}</td>
                </tr>
            )
        });
    }
    
    const onSelectUser = (e, user)=> { 
        const visitId = user.int_id_svisit;
        const visit = searchObjectInArrayByKeyVal(visits, 'p_id', visitId);
        open_modal(MAIN_PANEL_VIEW_VISIT_MODAL, [visit, user]);
    }
    
    
    const componentData = {
        pageIndicators: indicators,
        totalPages: totalNumOfPages,
        totalItems: numberOfVisitors,
        page,
        maxNumIndicators: 5,
        next,
        prev,
        setPage
    }

    return (
        <div className="custom-card" style={{height: "calc(100vh - 26.7rem)"}}>
                <div className="custom-card-header pl-4 d-flex justify-content-between align-items-center">
                    <span>{t("vis_in")}</span>
                    <Badge color="primary">{listOfVisitors.length}</Badge>
                </div>
                {listOfVisitors.length !== 0? <table className="w-100 table-striped table-sm table-borderless host-table">
                                                <thead>
                                                    <tr>
                                                        <th className="pl-4">{t('name')}</th>
                                                        <th>{t('id')}</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                {listOfVisitors}
                                                </tbody>
                                            </table> : 
                                            <p className="text-center w-100 my-4">{t('no_vis_in')}</p>}
                <div className="table-pages">
                <div className="separator"/>
                <div className="d-flex justify-content-between align-items-center  px-4">
                    <TableNavigation componentData={componentData}/>
                </div>
            </div>
        </div>
    )
}

VisitorsTable.propTypes = {
    open_modal: PropTypes.func.isRequired,
}

const mapStateToProps = (state) => ({
   
})

const mapDispatchToProps = {
    open_modal,
}

export default connect(mapStateToProps, mapDispatchToProps)(VisitorsTable)
