
import React, { Fragment } from 'react'
import { FormGroup, Input, Col } from 'reactstrap'
import { useTranslation } from 'react-i18next'
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { doesObjectExist } from '@utils/utils';


function HostsFastVisitDataPage2({flag, request, toEdit}) {
    const { t } = useTranslation('request');


    const generateMaterialsList = () => {
        const materials = doesObjectExist(request)? request.getAllMaterials(): [];
        return materials.map((mat, i) => {
          return (
            <FormGroup key={i} row>
              <Col md={5}>
                <Input
                  type="text"
                  name="material_name"
                  value={mat.p_oname}
                  onChange={(e) => mat.changeName(e.target.value)}
                  placeholder={t("name")}
                />
              </Col>
              <Col md={2}>
                <Input
                  type="text"
                  name="material_qtd"
                  value={mat.p_qnt}
                  onChange={(e) => mat.changeQuantity(e.target.value)}
                  placeholder={t("qtd")}
                />
              </Col>
              <Col md={2}>
                <span
                  name="accept"
                  className={`hoverable px-2 mt-1 d-inline-block btn-error`}
                  onClick={(e) => request.removeExtra(mat.p_oname)}>
                  {t("remove")}
                </span>
              </Col>
            </FormGroup>
          );
        });
      };
  
    const generateVehiclesList = () => {
      const vehicles = request? request.getAllVehicles() : [];
      return vehicles.map((ve, i) => {
      return (
          <FormGroup key={i} row>
              <Col md={5}>
                <Input
                    type="text"
                    name="vehicle"
                    value={ve.p_oname}
                    onChange={(e) => ve.changeName(e.target.value)}
                    placeholder={t("license")}
                />
              </Col>
              <Col md={2}>
                <span
                    name="remove"
                    className={`hoverable px-2 mt-1 d-inline-block btn-error`}
                    onClick={(e) => request.removeExtra(ve.p_oname)}>
                    {t("remove")}
                </span>
              </Col>
          </FormGroup>
          );
        });
      };

    return (
        <Fragment>
            <div className="my-4 d-block"></div>
                <div>
                    <p className="d-inline-block mx-2" col={1}> {t("material")} </p>
                    <span name="accept" className={`hoverable mx-2 px-2 d-inline-block btn-green`} onClick={(e) => request.addMaterial()} > {t("add_mat")} +{" "} </span>
                </div>
                {generateMaterialsList()}
                <div>
                    <p className="d-inline-block mx-2" col={1}> {t("vehicle")} </p>
                    <span name="accept" className={`hoverable mx-2 px-2 d-inline-block btn-green`} onClick={(e) => request.addVehicle()} > {t("add_veh")} +{" "} </span>
                </div>
                {generateVehiclesList()}
        </Fragment>
    )
}


HostsFastVisitDataPage2.propTypes = {
    flag: PropTypes.bool,
}

const mapStateToProps = (state) => ({
    flag: state.admin.flag,
})

const mapDispatchToProps = {
    
}


export default connect(mapStateToProps, mapDispatchToProps)(HostsFastVisitDataPage2);