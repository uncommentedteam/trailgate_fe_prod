import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { close_modal } from '@actions/admin'
import { useTranslation } from 'react-i18next'
import { Modal, ModalBody, Form, ModalFooter, Button } from 'reactstrap'
import HostsFastVisitDataPage1 from './HostsFastVisitDataPage1'
import HostsVisitDataPage3 from '../Shared/HostsVisitDataPage3'
import { fetchLoggedUserVisits, performHostFastRequestSubmissionProcess } from '@actions/visits'
import { CREATE_SERV_REQUEST_MODAL } from '@utils/constants'

const FastVisitModal = ({loggedUser, visit, performHostFastRequestSubmissionProcess, close_modal}) => {
    const {t} = useTranslation('request');
    const [state, setState] = useState({
        page: 0,
        numOfPages: 1,
        disSub: false,
    });

    const {page, numOfPages, disSub} = state;

    // ::: NAVIGATION
    const next = e => { setState({...state, page: page >= numOfPages? numOfPages : page + 1}); }
    const prev = e => { setState({...state, page: page <= 0? 0:page - 1}); }

    // ::: EVENT HANDLERS
    const onSubmit = async e => { 
        setState({...state, disSub: true});
        let isSuccessful = false; 
        isSuccessful = await performHostFastRequestSubmissionProcess(visit, loggedUser);
        if(isSuccessful) { 
            onClose();
            fetchLoggedUserVisits(loggedUser);
        }
        setState({...state, disSub: false});
    }

    const onClose = e => {
        close_modal(CREATE_SERV_REQUEST_MODAL);
    }

    // ::: HTML ELEMENTS
    const submitButtonText = page >= numOfPages? t('submit'):t('next');
    const submitButtonColor = `${page >= numOfPages? 'success':'primary'}`;
    
    return (
        <Modal isOpen={true} className="no-border" centered size="lg">
            {/* ::: MODAL TITLE */}
            <h3 className="title-of-modal">{t("cre_req")}</h3>
            <ModalBody className="modal-body paged-form-container" >
                <Form className="modal-paged-form">
                    {page === 0 && <HostsFastVisitDataPage1 visit={visit}/>}
                    {page === 1 && <HostsVisitDataPage3 visit={visit}/>}
                </Form>
                {/* ::: PAGE INDICATORS */}
                <div className="page-indicators-container blue-version">
                    <span className={`page-indicator ${page === 0 ? "active" : ""}`}></span>
                    <span className={`page-indicator ${page === 1 ? "active" : ""}`}></span>
                </div>
            </ModalBody>
            <ModalFooter>
                {/* ::: PAGED BUTTONS */}
            <div className="modal-footer-button-container">
                <Button className="left-sided-button" onClick={e => onClose()}>{t('cancel')}</Button>
                <Button className="right-sided-button" disabled={disSub} color={submitButtonColor}  onClick={e => page >= numOfPages? onSubmit(e):next(e)}>{submitButtonText}</Button>
                <Button className="right-sided-button"  color="primary" disabled={page <= 0} onClick={e => prev(e)}>{t('previous')}</Button>
            </div>
            </ModalFooter>
        </Modal>
    )
}

FastVisitModal.propTypes = {
    performHostFastRequestSubmissionProcess: PropTypes.func.isRequired,
    close_modal: PropTypes.func.isRequired,
    loggedUser: PropTypes.object,
    flag: PropTypes.bool,
}

const mapStateToProps = (state) => ({
    flag: state.admin.flag,
    loggedUser: state.auth.user,
})

const mapDispatchToProps = {
    performHostFastRequestSubmissionProcess,
    close_modal,
}

export default connect(mapStateToProps, mapDispatchToProps)(FastVisitModal)

