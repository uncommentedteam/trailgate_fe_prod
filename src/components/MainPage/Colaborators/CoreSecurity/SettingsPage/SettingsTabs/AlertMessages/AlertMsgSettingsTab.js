import React, { Fragment, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setMessage } from '@actions/toast'
import { open_modal } from '@actions/admin'
import AddAlertMsgModal from './AddAlertMsgModal'
import AlertMessageSettingsTable from './AlertMessageSettingsTable'
import AppFunctionalityUsable from '@model/Extras/AppFunctionalityUsable'



const AlertMsgSettingsTab = ({flag, setMessage, open_modal, modal_data}) => {
    const [state, setState] = useState({
        toShowAddModal: false,
    })

    // ::: EVENT HANDLERS
    const openAddModal = e => { 
        setState({...state, toShowAddModal: true});
    }

    const closeAddModal = e => { 
        setState({...state, toShowAddModal: false});
    }

    useEffect(() => { 
        AppFunctionalityUsable.getAlertMessages();
    }, [])

    return (
        <Fragment>
            <AddAlertMsgModal setMessage={setMessage}  toShow={state.toShowAddModal} closeModal={closeAddModal} />
            <AlertMessageSettingsTable modal_data={modal_data} open_modal={open_modal} openAddModal={openAddModal}/>
        </Fragment>
    )
}

AlertMsgSettingsTab.propTypes = {
    flag: PropTypes.bool,
    setMessage: PropTypes.func.isRequired,
    open_modal: PropTypes.func.isRequired,
    modal_data: PropTypes.object,
}

const mapStateToProps = (state) => ({
    flag: state.admin.flag,
    modal_data: state.admin.modal_data
})

const mapDispatchToProps = {
    setMessage,
    open_modal,
}

export default connect(mapStateToProps, mapDispatchToProps)(AlertMsgSettingsTab)
