import store from "../../store";
import { update } from "@actions/admin";

class NewInductionQuestion { 
    constructor(qid) {
        
        this.setUpQuestion();

    }

    set(key, val) {
        this[key] = val;
		store.dispatch(update());
	}

	get(key) {
		return this[key] || "";
	}    

}

export default NewInductionQuestion;