const { isEmptyArray, getMomentFromString, getToday} = require('./utils');
const {fixInputNames, fixInputEmails, fixInputPhoneNumbers, fixInputIdNumbers, fixNewUserFields, getCountryCodeByCountryName, validateName, validatePhoneNumbers, validateIdNumber, validatePasswords, validateTermsAndConditions, fixInputPeriod, fixInputDateTime} = require('./validations');

describe("TESTING fixInputNames: ", () => { 
    test('GIVEN an empty string THEN should return an empty string', () => { 
        expect(fixInputNames('')).toBe('');
    });
    test('GIVEN undefined THEN should return an empty string', () => { 
        expect(fixInputNames(undefined)).toBe('');
    });
    test('GIVEN null THEN should return an empty string', () => { 
        expect(fixInputNames(null)).toBe('');
    });
    test('GIVEN only aphabetic characters THEN should return the alphabetic characters', () => { 
        expect(fixInputNames("John Doe")).toBe('John Doe');
    });
    test('GIVEN only numeric characters THEN should throw error', () => { 
        expect(() => fixInputNames("123456")).toThrow(Error);
    });
    test('GIVEN only numeric characters with alphabetic THEN should throw error', () => { 
        expect(() => fixInputNames("jonh doe2")).toThrow(Error);
    });
    test('GIVEN only alphanumeric characters with special characters THEN should throw error', () => { 
        expect(() => fixInputNames("@AmazingGrace?*#")).toThrow(Error);
    });
});

describe("TESTING fixInputIdNumbers: ", () => { 
    test('GIVEN an empty string THEN should return an empty string', () => { 
        expect(fixInputIdNumbers('')).toBe('');
    });
    test('GIVEN undefined THEN should return an empty string', () => { 
        expect(fixInputIdNumbers(undefined)).toBe('');
    });
    test('GIVEN null THEN should return an empty string', () => { 
        expect(fixInputIdNumbers(null)).toBe('');
    });
    test('GIVEN only aphabetic characters THEN should return the alphabetic characters', () => { 
        expect(fixInputIdNumbers("John Doe")).toBe('John Doe');
    });
    test('GIVEN only numeric characters THEN should return what was inputed', () => { 
        expect(fixInputIdNumbers("123456")).toBe("123456");
    });
    test('GIVEN only numeric characters with alphabetic THEN should return what was inputed', () => { 
        expect(fixInputIdNumbers("jonh doe2")).toBe("jonh doe2");
    });
    test('GIVEN only numeric characters with alphabetic THEN should return what was inputed', () => { 
        expect(fixInputIdNumbers("092840qlkjaf90f32324I")).toBe("092840qlkjaf90f32324I");
    });
    test('GIVEN only alphanumeric characters with special characters THEN should throw error', () => { 
        expect(() => fixInputIdNumbers("@AmazingGrace?*#")).toThrow(Error);
    });
});

describe("TESTING fixInputEmails: ", () => { 
    test('GIVEN an empty string THEN should return an empty string', () => { 
        expect(fixInputEmails('')).toBe('');
    });
    test('GIVEN undefined THEN should return an empty string', () => { 
        expect(fixInputEmails(undefined)).toBe('');
    });
    test('GIVEN null THEN should return an empty string', () => { 
        expect(fixInputEmails(null)).toBe('');
    });
    test('GIVEN email with space THEN should return an email withoud spaces', () => { 
        expect(fixInputEmails("some one email@ gmail.com")).toBe('someoneemail@gmail.com');
    });
});

describe("TESTING fixInputPhoneNumbers: ", () => { 
    test('GIVEN an empty string THEN should return an empty string', () => { 
        expect(fixInputPhoneNumbers('')).toBe('');
    });
    test('GIVEN undefined THEN should return an empty string', () => { 
        expect(fixInputPhoneNumbers(undefined)).toBe('');
    });
    test('GIVEN null THEN should return an empty string', () => { 
        expect(fixInputPhoneNumbers(null)).toBe('');
    });
    test('GIVEN phone number THEN should return the phoneNumber', () => { 
        expect(fixInputPhoneNumbers("8433991092")).toBe('8433991092');
    });
    test('GIVEN phone number with country code (including the +) THEN should return the whole phoneNumber', () => { 
        expect(fixInputPhoneNumbers("+2588433991092")).toBe('+2588433991092');
    });
    test('GIVEN phone number with special character at index 1 THEN should throw error', () => { 
        expect(() => fixInputPhoneNumbers("2+588433991092")).toThrow(Error);
    });
    test('GIVEN input with alphanumeric characters THEN should throw', () => { 
        expect(() => fixInputPhoneNumbers("029409234myNumber")).toThrow(Error);
    });
    test('GIVEN phone number with special characters THEN should throw', () => { 
        expect(() => fixInputPhoneNumbers("84-3399-1092")).toThrow(Error);
    });
    test('GIVEN only alphabetic characters THEN should return throw', () => { 
        expect(() => fixInputPhoneNumbers("absclkjaf")).toThrow(Error);
    });
});

describe("TESTING fixNewUserFields: ", () => { 
    test('GIVEN an empty object THEN should return object with keys', () => {
        const user = fixNewUserFields({});
        expect(isEmptyArray(Object.keys(user))).toBeFalsy();
    });
    test('GIVEN an empty object THEN should return object with keys that have empty strings', () => {
        const user = fixNewUserFields({});
        Object.keys(user).forEach(key => {
            expect(user[key]).toBe('');
        })
    });
    test('GIVEN an object with some empty fields THEN should return object with some keys that have empty strings', () => {
        const user = fixNewUserFields({p_fname: "name", p_email:"myemail@gmail.com"});
        Object.keys(user).forEach(key => {
            if(key === "p_fname")
                expect(user[key]).toBe("name");
            else if(key === "p_email")
                expect(user[key]).toBe("myemail@gmail.com");
            else
                expect(user[key]).toBe('');
        })
    });
});


describe("TESTING getCountryCodeByCountryName: ", () => { 
    test('GIVEN niger THEN should return +227', () => {
        expect(getCountryCodeByCountryName('Niger')).toBe('+227');
    });
    test('GIVEN France THEN should return +33', () => {
        expect(getCountryCodeByCountryName('France')).toBe('+33');
    });
    test('GIVEN South Africa THEN should return +33', () => {
        expect(getCountryCodeByCountryName('South Africa')).toBe('+27');
    });
    test('GIVEN empty string THEN should return +258', () => {
        expect(getCountryCodeByCountryName('')).toBe('+258');
    });
    test('GIVEN non existent coutry THEN should return +258', () => {
        expect(getCountryCodeByCountryName('MyCoutnry')).toBe('+258');
    });
    test('GIVEN Mozambique coutry THEN should return +258', () => {
        expect(getCountryCodeByCountryName('Mozambique')).toBe('+258');
    });
});


describe("TESTING validateName: ", () => { 
    test('GIVEN an empty string THEN should throw error', () => { 
        expect(() => validateName('')).toThrow(Error);
    });
    test('GIVEN undefined THEN should throw errorg', () => { 
        expect(() => validateName(undefined)).toThrow(Error);
    });
    test('GIVEN null THEN  should throw error', () => { 
        expect(() => validateName(null)).toThrow(Error);
    });
    test('GIVEN only aphabetic characters THEN should return the alphabetic characters', () => { 
        expect(validateName("John Doe")).toBeTruthy();
    });
    test('GIVEN only numeric characters THEN should throw error', () => { 
        expect(() => validateName("123456")).toThrow(Error);
    });
    test('GIVEN only 2 aphabetic characters THEN should throw error', () => { 
        expect(() => validateName("jb")).toThrow(Error);
    });
    test('GIVEN only space characters THEN should throw error', () => { 
        expect(() => validateName("   ")).toThrow(Error);
    });
    test('GIVEN  numeric characters with alphabetic THEN should throw error', () => { 
        expect(() => validateName("jonh doe2")).toThrow(Error);
    });
    test('GIVEN only alphanumeric characters with special characters THEN should throw error', () => { 
        expect(() => validateName("@AmazingGrace?*#")).toThrow(Error);
    });
});

describe("TESTING validatePhoneNumbers: ", () => { 
    test('GIVEN an empty string THEN should throw error', () => { 
        expect(() => validatePhoneNumbers('')).toThrow(Error);
    });
    test('GIVEN undefined THEN should throw errorg', () => { 
        expect(() => validatePhoneNumbers(undefined)).toThrow(Error);
    });
    test('GIVEN null THEN  should throw error', () => { 
        expect(() => validatePhoneNumbers(null)).toThrow(Error);
    });
    test('GIVEN the same phone numbers THEN should throw error', () => { 
        expect(() => validatePhoneNumbers('+2588433898', '+2588433898')).toThrow(Error);
    });
    test('GIVEN different phone numbers (with country codes) THEN should return true', () => { 
        expect(validatePhoneNumbers("+2838844759", "+2582288989")).toBeTruthy();
    });
    test('GIVEN different phone numbers (without country codes) THEN should return true', () => { 
        expect(validatePhoneNumbers("838844759", "82288989")).toBeTruthy();
    });
    test('GIVEN only phone number 1 THEN should throw error', () => { 
        expect(() => validatePhoneNumbers("+2039929230")).toThrow(Error);
    });
    test('GIVEN only phone number 1 THEN should throw error', () => { 
        expect(() => validatePhoneNumbers("","+2039929230")).toThrow(Error);
    });
});

describe("TESTING validateIdNumbers: ", () => { 
    test('GIVEN an empty string THEN should throw error', () => { 
        expect(() => validateIdNumber('')).toThrow(Error);
    });
    test('GIVEN undefined THEN should throw errorg', () => { 
        expect(() => validateIdNumber(undefined)).toThrow(Error);
    });
    test('GIVEN null THEN  should throw error', () => { 
        expect(() => validateIdNumber(null)).toThrow(Error);
    });
    test('GIVEN an id number THEN should return true', () => { 
        expect(validateIdNumber("qoirueqo302984adf")).toBeTruthy();
    });
});

describe.only("TESTING validatePasswords: ", () => { 
    test('GIVEN an empty string THEN should throw error', () => { 
        expect(() => validatePasswords('')).toThrow(Error);
    });
    test('GIVEN undefined THEN should throw errorg', () => { 
        expect(() => validatePasswords(undefined)).toThrow(Error);
    });
    test('GIVEN null THEN  should throw error', () => { 
        expect(() => validatePasswords(null)).toThrow(Error);
    });
    test('GIVEN the different password THEN should throw error', () => { 
        expect(() => validatePasswords('pass898', '+pass88433898')).toThrow(Error);
    });
    test('GIVEN same passwords  THEN should return true', () => { 
        expect(validatePasswords("pass38844759", "pass38844759")).toBeTruthy();
    });
    test('GIVEN only password1 THEN should throw error', () => { 
        expect(() => validatePasswords("83pass8844759")).toThrow(Error);
    });
    test('GIVEN only password2 THEN should throw error', () => { 
        expect(() => validatePasswords('', "pass9929230")).toThrow(Error);
    });
    test('GIVEN only password1 with spaces THEN should trhow error', () => { 
        expect(() => validatePasswords('       ', '       ')).toThrow(Error);
    })
});

describe("TESTING validateTermsAndConditions: ", () => { 
    test('GIVEN an empty string THEN should throw error', () => { 
        expect(() => validateTermsAndConditions('')).toThrow(Error);
    });
    test('GIVEN undefined THEN should throw errorg', () => { 
        expect(() => validateTermsAndConditions(undefined)).toThrow(Error);
    });
    test('GIVEN null THEN  should throw error', () => { 
        expect(() => validateTermsAndConditions(null)).toThrow(Error);
    });
    test('GIVEN true THEN should return true', () => { 
        expect(validateTermsAndConditions(true)).toBeTruthy();
    });
    test('GIVEN false THEN should throw error', () => { 
        expect(() => validateTermsAndConditions(false)).toThrow(Error);
    });
    test('GIVEN 1 THEN should return true', () => { 
        expect(validateTermsAndConditions(1)).toBeTruthy();
    });
});

describe("TESTING fixInputPeriod: ", () => { 
    test('GIVEN an empty string THEN should throw error', () => {
        expect(() => fixInputPeriod('')).toThrow(Error);
    });
    test('GIVEN undefined THEN should throw errorg', () => { 
        expect(() => fixInputPeriod(undefined)).toThrow(Error);
    });
    test('GIVEN null THEN  should throw error', () => {
        expect(() => fixInputPeriod(null)).toThrow(Error);
    });
    test('GIVEN period out of range (<=0) THEN should throw error', () => { 
        expect(() => fixInputPeriod(0)).toThrow(Error);
    });
    test('GIVEN period out of range (>=MAX_TIME_PERIOD) THEN should throw error', () => {
        expect(() => fixInputPeriod(90)).toThrow(Error);
    });
    test('GIVEN period within range THEN should return value', () => { 
        let value = 32;
        fixInputPeriod(value);
        expect(value).toBe(32)
    });
});

describe("TESTING fixInputDateTime: ", () => { 
    test('GIVEN no date but time THEN should return current date with default time', () => {
        let date1 = fixInputDateTime(null, "14:15");
        let date2 = fixInputDateTime(undefined, "01:13");
        let date3 = fixInputDateTime('', "21:39");
        const today = getToday();

        date1 = getMomentFromString(date1);
        date2 = getMomentFromString(date2);
        date3 = getMomentFromString(date3);

        expect(date1.date()).toBe(today.date());
        expect(date1.hour()).toBe(14);
        expect(date1.minute()).toBe(15);

        expect(date2.date()).toBe(today.date());
        expect(date2.hour()).toBe(1);
        expect(date2.minute()).toBe(13);
        
        expect(date3.date()).toBe(today.date());
        expect(date3.hour()).toBe(21);
        expect(date3.minute()).toBe(39);
    });
    test('GIVEN no time but date THEN should return the date with default time', () => {
        const today1 = getToday();
        const today2 = getToday().add(3, 'days');
        const today3 = getToday().subtract(10, 'days');
        
        let date1 = fixInputDateTime(today1.toISOString(), null);
        let date2 = fixInputDateTime(today2.toISOString(), undefined);
        let date3 = fixInputDateTime(today3.toISOString(), "");
        

        date1 = getMomentFromString(date1);
        date2 = getMomentFromString(date2);
        date3 = getMomentFromString(date3);

        expect(date1.date()).toBe(today1.date());
        expect(date1.hour()).toBe(8);
        expect(date1.minute()).toBe(0);

        expect(date2.date()).toBe(today2.date());
        expect(date2.hour()).toBe(8);
        expect(date2.minute()).toBe(0);
        
        expect(date3.date()).toBe(today3.date());
        expect(date3.hour()).toBe(8);
        expect(date3.minute()).toBe(0);
    });
    test('GIVEN date and time THEN should return the date with the time', () => {
        const today1 = getToday();
        const today2 = getToday().add(3, 'days');
        const today3 = getToday().subtract(10, 'days');
        
        let date1 = fixInputDateTime(today1.toISOString(), "14:15");
        let date2 = fixInputDateTime(today2.toISOString(), "01:13");
        let date3 = fixInputDateTime(today3.toISOString(), "21:39");

        date1 = getMomentFromString(date1);
        date2 = getMomentFromString(date2);
        date3 = getMomentFromString(date3);

        expect(date1.date()).toBe(today1.date());
        expect(date1.hour()).toBe(14);
        expect(date1.minute()).toBe(15);

        expect(date2.date()).toBe(today2.date());
        expect(date2.hour()).toBe(1);
        expect(date2.minute()).toBe(13);
        
        expect(date3.date()).toBe(today3.date());
        expect(date3.hour()).toBe(21);
        expect(date3.minute()).toBe(39);
    });
});
