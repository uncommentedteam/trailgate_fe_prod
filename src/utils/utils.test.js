const { DEFAULT_DATE_FORMAT_FOR_API_REQUESTS } = require('@actions/types');
const {isEmptyValue, getMomentFromStringWithFormat, isPhoneNumberValue, isPhoneNumberRegex, isPasswordValue, isEmailValue, isEmptyObject, isUndefinedOrNull, isOnlyWords, isOnlyNum, validateObjectParameter, validateArrayParameter, removeObjectFields, isVisitorUser, isServiceProviderUser, isHostUser, isThreadUser, isCoreSecurityUser, getDefaultVisitsTimeFrame} = require('./utils');

describe("TESTING isEmptyValue: ", () => { 
    test('[isEmptyValue] : GIVEN an empty string THEN it should be true', () => { 
        expect(isEmptyValue('')).toBeTruthy();
    });
    
    test('[isEmptyValue] : GIVEN null THEN it should be true', () => { 
        expect(isEmptyValue(null)).toBeTruthy();
    });
    
    test('[isEmptyValue] : GIVEN undefined THEN it should be true', () => { 
        expect(isEmptyValue(undefined)).toBeTruthy();
    });
    
    test('[isEmptyValue] : GIVEN empty object THEN it should be false', () => { 
        expect(isEmptyValue({})).toBeFalsy();
    });
    
    test('[isEmptyValue] : GIVEN empty array THEN it should be false', () => { 
        expect(isEmptyValue([])).toBeFalsy();
    });
    
    test('[isEmptyValue] : GIVEN a string THEN it should be false', () => { 
        expect(isEmptyValue("hello")).toBeFalsy();
    });
    
    test('[isEmptyValue] : GIVEN a number THEN it should be false', () => { 
        expect(isEmptyValue(1234)).toBeFalsy();
    });
})


describe("TESTING isPhoneNumberValue: ", () => {
    test('[isPhoneNumberValue] : GIVEN an alphanumeric string THEN it should throw error', () => { 
        expect(() => isPhoneNumberValue('2309jfa0f923001')).toThrow(Error);
    });
    test('[isPhoneNumberValue] : GIVEN only non numeric string THEN it should throw error', () => { 
        expect(() => isPhoneNumberValue('helloThisisMyNumber')).toThrow(Error);
    });
    test('[isPhoneNumberValue] : GIVEN numeric string with a in the beggining THEN it should throw error', () => { 
        expect(() => isPhoneNumberValue('a25877227387')).toThrow(Error);
    });   
    test('[isPhoneNumberValue] : GIVEN numeric string with + in the middle THEN it should throw error', () => { 
        expect(() => isPhoneNumberValue('258772+27387')).toThrow(Error);
    });
    test('[isPhoneNumberValue] : GIVEN numeric string without + in the beggining THEN it should not throw error', () => { 
        expect(() => isPhoneNumberValue('25877227387')).not.toThrow(Error);
    });
    test('[isPhoneNumberValue] : GIVEN numeric string with + in the beggining THEN it should not throw error', () => { 
        expect(() => isPhoneNumberValue('+25877227387')).not.toThrow(Error);
    });
    test('[isPhoneNumberValue] : GIVEN numeric string with spaces THEN it should not throw error', () => { 
        expect(() => isPhoneNumberValue('+258 87 722 73 87')).not.toThrow(Error);
    });
});

describe("TESTING isPasswordValue", () => { 
    test('GIVEN an empty string THEN it should throw an error', () => { 
        expect(() => isPasswordValue('')).toThrow();
    });
    test('GIVEN an alphanumeric string THEN it should not throw an error', () => { 
        expect(() => isPasswordValue('lakfjl113123jkas')).not.toThrow(Error);
    });
    test('GIVEN a no numeric string THEN it should not throw an error', () => { 
        expect(() => isPasswordValue('akdjflkjsldkfj')).not.toThrow(Error);
    });
    test('GIVEN a numeric string THEN it should not throw an error', () => { 
        expect(() => isPasswordValue('32424234244')).not.toThrow(Error);
    });
})

describe("TESTING isPhoneNumberRegex: ", () =>{ 
    test('GIVEN an alphanumeric string THEN it should be false', () => { 
        expect(isPhoneNumberRegex("20834029adlf0293sifj")).toBeFalsy();
    });
    test('GIVEN only non numeric string THEN it should be false', () => { 
        expect(isPhoneNumberRegex("helloThisisMyNumber")).toBeFalsy();
    });
    test('GIVEN numeric string with + THEN it should be false', () => { 
        expect(isPhoneNumberRegex("+28711882398")).toBeFalsy();
    });
    test('GIVEN numeric string without + THEN it should be true', () => { 
        expect(isPhoneNumberRegex("28711882398")).toBeTruthy();
    });
})

describe("TESTING isEmailValue: ", () => { 
    test('GIVEN an email @gmail.com THEN should throw error', () => { 
        expect(() => isEmailValue('@gmail.com')).toThrow(Error);
    });
    test('GIVEN an email gmail.com THEN should throw error', () => { 
        expect(() => isEmailValue('gmail.com')).toThrow(Error);
    });
    test('GIVEN an email myemail THEN should throw error', () => { 
        expect(() => isEmailValue('gmail.com')).toThrow(Error);
    });
    test('GIVEN an email myemail@smth THEN should throw error', () => { 
        expect(() => isEmailValue('myemail@smth')).toThrow(Error);
    });
    test('GIVEN an email myema il@smth THEN should throw error', () => { 
        expect(() => isEmailValue('myema il@smth')).toThrow(Error);
    });
    test('GIVEN an email myemail@smth.com.sub THEN should not throw error', () => { 
        expect(() => isEmailValue('myemail@smth.com.sub')).not.toThrow(Error);
    });
    test('GIVEN an email myemail@smth.com THEN should not throw error', () => { 
        expect(() => isEmailValue('myemail@smth.com')).not.toThrow(Error);
    });
})

describe("TESTING isEmptyObject: ", () =>{ 
    test('GIVEN an empty object THEN it should be true', () => { 
        expect(isEmptyObject({})).toBeTruthy();
    });
    test('GIVEN undefined THEN it should be true', () => { 
        expect(isEmptyObject(undefined)).toBeTruthy();
    });
    test('GIVEN null THEN it should be true', () => { 
        expect(isEmptyObject(null)).toBeTruthy();
    });
    test('GIVEN non empty object THEN it should be false', () => { 
        expect(isEmptyObject({property: 'value'})).toBeFalsy();
    });
});

describe("TESTING isUndefinedOrNull: ", () =>{ 
    test('GIVEN an empty object or array THEN it should be false', () => { 
        expect(isUndefinedOrNull({})).toBeFalsy();
        expect(isUndefinedOrNull([])).toBeFalsy();
    });
    test('GIVEN undefined THEN it should be true', () => { 
        expect(isUndefinedOrNull(undefined)).toBeTruthy();
    });
    test('GIVEN null THEN it should be true', () => { 
        expect(isUndefinedOrNull(null)).toBeTruthy();
    });
    test('GIVEN string or number THEN it should be false', () => { 
        expect(isUndefinedOrNull('someString')).toBeFalsy();
        expect(isUndefinedOrNull(1234)).toBeFalsy();
    });
});

describe("TESTING isOnlyWords: ", () =>{ 
    test('GIVEN only string of numbers THEN it should be false', () => { 
        expect(isOnlyWords("1209123")).toBeFalsy();
    });
    test('GIVEN only string of words THEN it should be true', () => { 
        expect(isOnlyWords("hello")).toBeTruthy();
    });
    test('GIVEN only string of numbers and letters THEN it should be false', () => { 
        expect(isOnlyWords("0322f2fj02je2")).toBeFalsy();
    });
    test('GIVEN only string of words with special characters THEN it should be false', () => { 
        expect(isOnlyWords('lakjf?&)#lksf')).toBeFalsy();
    });
    test('GIVEN only string of words with number and a space  THEN it should be false', () => { 
        expect(isOnlyWords('jonh doe2')).toBeFalsy();
    });
});

describe("TESTING isOnlyNum: ", () =>{ 
    test('GIVEN only string of numbers THEN it should be true', () => { 
        expect(isOnlyNum("1209123")).toBeTruthy();
    });
    test('GIVEN only string of words THEN it should be false', () => { 
        expect(isOnlyNum("hello")).toBeFalsy();
    });
    test('GIVEN only string of numbers and letters THEN it should be false', () => { 
        expect(isOnlyNum("0322f2fj02je2")).toBeFalsy();
    });
    test('GIVEN only string of words with special characters THEN it should be false', () => { 
        expect(isOnlyNum('lakjf?&)#lksf')).toBeFalsy();
    });
    test('GIVEN only string of words with number and a space  THEN it should be false', () => { 
        expect(isOnlyNum('jonhdoe2f')).toBeFalsy();
    });
    test('GIVEN only string of words with number and a space  THEN it should be false', () => { 
        expect(isOnlyNum('jonhdoe2')).toBeFalsy();
    });
    test('GIVEN only string of words with number and a space  THEN it should be false', () => { 
        expect(isOnlyNum('jonh doe2')).toBeFalsy();
    });
});

describe("TESTING validateObjectParameter: ", () =>{ 
    test('GIVEN empty object THEN it should return empty object', () => { 
        const returnedObject = validateObjectParameter({});
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN undefined THEN it should return empty object', () => { 
        const returnedObject = validateObjectParameter(undefined);
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN null THEN it should return empty object', () => { 
        const returnedObject = validateObjectParameter(null);
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN object THEN it should return the object', () => { 
        const returnedObject = validateObjectParameter({hello: 'hello'});
        expect(Object.keys(returnedObject).length).toBe(1);
    });
});

describe("TESTING validateArrayParameter: ", () =>{ 
    test('GIVEN empty object THEN it should return empty object', () => { 
        const returnedObject = validateArrayParameter([]);
        expect(returnedObject.length).toBe(0);
    });
    test('GIVEN undefined THEN it should return empty object', () => { 
        const returnedObject = validateArrayParameter(undefined);
        expect(returnedObject.length).toBe(0);
    });
    test('GIVEN null THEN it should return empty object', () => { 
        const returnedObject = validateArrayParameter(null);
        expect(returnedObject.length).toBe(0);
    });
    test('GIVEN object THEN it should return the object', () => { 
        const returnedObject = validateArrayParameter(['hello']);
        expect(returnedObject.length).toBe(1);
    });
});


describe("TESTING removeObjectFields: ", () =>{ 
    test('GIVEN empty object THEN it should return empty object', () => { 
        const returnedObject = removeObjectFields({});
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN undefined THEN it should return empty object', () => { 
        const returnedObject = removeObjectFields(undefined);
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN null THEN it should return empty object', () => { 
        const returnedObject = removeObjectFields(null);
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN object with one field THEN it should return an empty object', () => { 
        const returnedObject = removeObjectFields({hello: 'hello'}, ['hello']);
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN object with two fields being one filtered both THEN it should return an empty object', () => { 
        const returnedObject = removeObjectFields({hello: 'hello', yellow:'yellow'}, ['hello', 'yellow']);
        expect(returnedObject.yellow).not.toBe('yellow');
        expect(Object.keys(returnedObject).length).toBe(0);
    });
    test('GIVEN object with two fields being one filtered THEN it should return an object with one key', () => { 
        const returnedObject = removeObjectFields({hello: 'hello', yellow:'yellow'}, ['hello']);
        expect(returnedObject.yellow).toBe('yellow');
        expect(Object.keys(returnedObject).length).toBe(1);
    });
});


describe("TESTING userIdentification: ", () => { 
    test('GIVEN an visitor object THEN show return true for it and false for others', () => { 
        const userObject = {
            "p_id": 4718,
            "p_fname": "James",
            "p_lname": "Bond1",
            "p_nid": "jamesbond1ID",
            "p_psw": "7KKL6G1G",
            "p_id_provenance": null,
            "p_email": null,
            "p_pnumber_1": "8400000071",
            "p_pnumber_2": "8200000071",
            "p_nationality": "Malaysia",
            "p_id_type_id": 143,
            "p_expiration_date_id": "2020-11-30T00:00:00.000Z",
            "entity": null,
            "p_istemp": 1,
            "p_active": 1,
            "p_isvisitor": 1,
            "p_registry": "2020-11-21T15:33:30.842Z",
            "p_tokken": "Jo0Kq4K6TU9gdsrznajLAGtDX5uPuBNc",
            "p_id_entity": null,
            "p_isblocked": false
        }

        expect(isVisitorUser(userObject)).toBeTruthy();
        expect(isServiceProviderUser(userObject)).toBeFalsy();
        expect(isHostUser(userObject)).toBeFalsy();
        expect(isThreadUser(userObject)).toBeFalsy();
        expect(isCoreSecurityUser(userObject)).toBeFalsy();
    });
    test('GIVEN an service provider object THEN show return true for it and false for others', () => { 
        const userObject = {
            "p_id": 26783,
            "p_fname": "CompuserA",
            "p_lname": "Compuser",
            "p_nid": "93820480234",
            "p_psw": "123456",
            "p_id_provenance": null,
            "p_email": "compusera@gmail.com",
            "p_pnumber_1": "+2583241534143",
            "p_pnumber_2": "+25815143131413",
            "p_nationality": "Mozambique",
            "p_id_type_id": 47,
            "p_expiration_date_id": "2021-06-04T00:00:00.000Z",
            "entity": "DunderMifflin",
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 1,
            "p_registry": "2021-05-20T19:22:48.449Z",
            "p_tokken": "IxQlyFKdvNEfLgrGgSJkafNDwfm3eTpN",
            "p_id_entity": 8181,
            "p_isblocked": null,
            "is_rep": true
        }

        expect(isVisitorUser(userObject)).toBeFalsy();
        expect(isServiceProviderUser(userObject)).toBeTruthy();
        expect(isHostUser(userObject)).toBeFalsy();
        expect(isThreadUser(userObject)).toBeFalsy();
        expect(isCoreSecurityUser(userObject)).toBeFalsy();
    });
    test('GIVEN a host object THEN show return true for it and false for others', () => { 
        const userObject = {
            "p_id": 15,
            "p_fname": "Leila",
            "p_lname": "Damons",
            "p_nid": "OLDCHEVY",
            "p_psw": "123456",
            "p_email": "Leila@gmail.com",
            "p_pnumber_1": "847931877",
            "p_pnumber_2": "847931872",
            "p_nationality": "Mozambique",
            "p_id_type_id": 143,
            "p_expiration_date_id": "2022-02-02T00:00:00.000Z",
            "company": "MPDC",
            "department": "IT",
            "p_id_role": 51,
            "p_id_department": 4,
            "p_id_company": 4,
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 0,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQ11"
        }

        expect(isVisitorUser(userObject)).toBeFalsy();
        expect(isServiceProviderUser(userObject)).toBeFalsy();
        expect(isHostUser(userObject)).toBeTruthy();
        expect(isThreadUser(userObject)).toBeFalsy();
        expect(isCoreSecurityUser(userObject)).toBeFalsy();
    });
    test('GIVEN a thread object THEN show return true for it and false for others', () => { 
        const userObject = {
            "p_id": 8,
            "p_fname": "Jeronimo",
            "p_lname": "Tameles",
            "p_nid": "ABCDZ1F",
            "p_psw": "123456",
            "p_email": "Jeronim@gmail.com",
            "p_pnumber_1": "847931874",
            "p_pnumber_2": "847931872",
            "p_nationality": "Mozambique",
            "p_id_type_id": 49,
            "p_expiration_date_id": "2021-04-02T00:00:00.000Z",
            "company": "MPDC",
            "department": "Segurança",
            "p_id_role": 52,
            "p_id_department": 9,
            "p_id_company": 4,
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 0,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQh0"
        }

        expect(isVisitorUser(userObject)).toBeFalsy();
        expect(isServiceProviderUser(userObject)).toBeFalsy();
        expect(isHostUser(userObject)).toBeFalsy();
        expect(isThreadUser(userObject)).toBeTruthy();
        expect(isCoreSecurityUser(userObject)).toBeFalsy();
    });
    test('GIVEN a coresecurity object THEN show return true for it and false for others', () => { 
        const userObject = {
            "p_id": 9,
            "p_fname": "Isac",
            "p_lname": "Massale",
            "p_nid": "CASRR4FXFF",
            "p_psw": "123456",
            "p_email": "Isac@gmail.com",
            "p_pnumber_1": "847931875",
            "p_pnumber_2": "847931874",
            "p_nationality": "Mozambique",
            "p_id_type_id": 50,
            "p_expiration_date_id": "2021-12-12T00:00:00.000Z",
            "company": "MPDC",
            "department": "Segurança",
            "p_id_role": 53,
            "p_id_department": 9,
            "p_id_company": 4,
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 0,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQ10"
        }

        expect(isVisitorUser(userObject)).toBeFalsy();
        expect(isServiceProviderUser(userObject)).toBeFalsy();
        expect(isHostUser(userObject)).toBeFalsy();
        expect(isThreadUser(userObject)).toBeFalsy();
        expect(isCoreSecurityUser(userObject)).toBeTruthy();
    });
    test('GIVEN an unknown object THEN show return false', () => { 
        const userObject = {
            "p_id": 9,
            "p_fname": "Isac",
            "p_lname": "Massale",
            "p_nid": "CASRR4FXFF",
            "p_psw": "123456",
            "p_email": "Isac@gmail.com",
            "p_pnumber_1": "847931875",
            "p_pnumber_2": "847931874",
            "p_nationality": "Mozambique",
            "p_id_type_id": 50,
            "p_expiration_date_id": "2021-12-12T00:00:00.000Z",
            "company": "MPDC",
            "department": "Segurança",
            "p_id_department": 9,
            "p_id_company": 4,
            "p_active": 1,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQ10"
        }

        expect(isVisitorUser(userObject)).toBeFalsy();
        expect(isServiceProviderUser(userObject)).toBeFalsy();
        expect(isHostUser(userObject)).toBeFalsy();
        expect(isThreadUser(userObject)).toBeFalsy();
        expect(isCoreSecurityUser(userObject)).toBeFalsy();
    });
});

describe("TESTING getDefaultVisitsTimeFrame: ", () =>{ 
    test('GIVEN nothing THEN it should return non empty Object', () => { 
        const visitsTimeFrame = getDefaultVisitsTimeFrame();
        expect(Object.keys(visitsTimeFrame).length).toBe(2);
        expect(isEmptyValue(visitsTimeFrame.p_dtm_start)).toBeFalsy();
        expect(isEmptyValue(visitsTimeFrame.p_dtm_end)).toBeFalsy();
    });
    test('GIVEN nothing THEN start date should be before end date', () => { 
        const visitsTimeFrame = getDefaultVisitsTimeFrame();
        const startDate = getMomentFromStringWithFormat(visitsTimeFrame.p_dtm_start, DEFAULT_DATE_FORMAT_FOR_API_REQUESTS);
        const endDate = getMomentFromStringWithFormat(visitsTimeFrame.p_dtm_end, DEFAULT_DATE_FORMAT_FOR_API_REQUESTS);
        
        expect(startDate.isBefore(endDate)).toBeTruthy();
    });
});