const { CREATE_REQUEST_PRIV, ADD_VISITOR_PRIV, CREATE_FAST_REQ_PRIV, CREATE_SERV_REQ_PRIV, MAIN_PANEL_PRIV, GRANT_ACCESS_PRIV, BLOCK_USER_PRIV, TERMINAL_PANEL_PRIV, REPORTS_PRIV, SETTINGS_PRIV } = require("@utils/constants");
const { sendRequestDataWithDefaultConfig, throwErrorIfApiResponseIsEmpty, checkApiResponseForErrors, isEmptyValue } = require("@utils/utils");
const { default: store } = require("../store");
const { getNewUserObject, getNewCompanyUserObject, assignPrivilegesAccordingToUserType, getNewHostObject, addUserDefaultFields, addDummyPasswordToUser, getNewFastRequestUserObject, getNewCompanyObject } = require("./auth");
const crypto = require('crypto');

const  ROOT_URL  = "http://localhost:3000/api"
jest.setTimeout(15000);

describe("TESTING login action: ", () => { 
   test("GIVEN correct email and password THEN should return user with correct email", async () => {
       const user = await sendRequestDataWithDefaultConfig(`${ROOT_URL}/auth/login`, {p_email_pnumber: "isac@gmail.com", p_psw: "123456", p_isvisitor: 1});
       const sha256 = crypto.createHash('sha256');
       expect(user.email.toLowerCase()).toBe("isac@gmail.com");
       expect(user.psw).toBe(sha256.update("123456").digest('hex'));
   });
   test("GIVEN correct email and wrong password THEN response should throw Error", async () => { 
    const response = await sendRequestDataWithDefaultConfig(`${ROOT_URL}/auth/login`, {p_email_pnumber: "isac@gmail.com", p_psw: "1234562342", p_isvisitor: 1});
    expect(() => { 
        throwErrorIfApiResponseIsEmpty(response, "Username or Password not valid!");
        checkApiResponseForErrors(response);
       }).toThrow("Username or Password not valid!");
   });
   test("GIVEN wrong email and correct password THEN response should throw Error", async () => { 
    const response = await sendRequestDataWithDefaultConfig(`${ROOT_URL}/auth/login`, {p_email_pnumber: "isacs@gmail.com", p_psw: "123456", p_isvisitor: 1});
    expect(() => { 
        throwErrorIfApiResponseIsEmpty(response, "Username or Password not valid!");
        checkApiResponseForErrors(response);
       }).toThrow("Username or Password not valid!");
   });
});

describe("TESTING NEW USER AND OBJECTS generators: ", () => { 

    const testIfContainsNewUserDefaultFields = (userObject) => { 
        expect(userObject.p_registry).not.toBeNull();
        expect(userObject.p_registry).not.toBeUndefined();
        expect(userObject.p_nationality).not.toBeNull();
        expect(userObject.p_nationality).not.toBeUndefined();
        expect(userObject.p_id_type_id).not.toBeNull();
        expect(userObject.p_id_type_id).not.toBeUndefined();
        expect(userObject.p_isvisitor).not.toBeNull();
        expect(userObject.p_isvisitor).not.toBeUndefined();
        expect(userObject.p_istemp).not.toBeNull();
        expect(userObject.p_istemp).not.toBeUndefined();
        expect(userObject.p_active).not.toBeNull();
        expect(userObject.p_active).not.toBeUndefined();
    }

    const testIfContainsFieldsForNewUserObjecs = (userObject) => { 
        testIfContainsNewUserDefaultFields(userObject);
        expect(isEmptyValue(userObject.terms_condition)).toBeFalsy();
        expect(isEmptyValue(userObject.terms_condition)).toBeFalsy();
    }

    const testIfUserDoesNotHavePasswordDefined = (userObject) => { 
        expect(userObject.p_psw).toBeUndefined();
        expect(userObject.p_psw_2).toBeUndefined();
    }

    const testIfUserDoesHavePasswordDefined = (userObject) => { 
        expect(userObject.p_psw).not.toBeNull();
        expect(userObject.p_psw).not.toBeUndefined();
        expect(userObject.p_psw_2).not.toBeNull();
        expect(userObject.p_psw_2).not.toBeUndefined();
    }

    const testIfNewUserDoesNotHaveNewProvenance = (userObject) => { 
        expect(isEmptyValue(userObject.new_provenance)).toBeFalsy();
        expect(isEmptyValue(userObject.new_provenance)).toBeFalsy();
    }

    test('[addNewUserDefaultFields] GIVEN an emtpy object THEN it should add all USER DEFAULT FIELDS', () => { 
        const emptyObject = {};
        addUserDefaultFields(emptyObject);
        testIfUserDoesNotHavePasswordDefined(emptyObject);
    });
    test('[getNewUserObject] GIVEN the need for a new simple user THEN should generate non empty new user object with all fields of a NEW USER', () => { 
        const newUser = store.dispatch(getNewUserObject());
        testIfContainsFieldsForNewUserObjecs(newUser);
        testIfUserDoesNotHavePasswordDefined(newUser);
        testIfNewUserDoesNotHaveNewProvenance(newUser);
    });
    test('[getNewFastRequestUserObject] GIVEN the need for a new fast request user THEN should generate non empty new user object with all fields of a NEW  FAST REQUEST USER', () => { 
        const newUser = store.dispatch(getNewFastRequestUserObject());
        testIfContainsFieldsForNewUserObjecs(newUser);
        testIfUserDoesHavePasswordDefined(newUser);
        testIfNewUserDoesNotHaveNewProvenance(newUser);
        expect(newUser.p_istemp).toBe(1);
        expect(newUser.p_is_toEmail).toBe(1);
    });
    test('[getNewCompanyUserObject] GIVEN the need for a new Company User Object THEN should generate non empty new user object with all fields of a new COMPANY USER', () => { 
        const newCompanyUserObject = store.dispatch(getNewCompanyUserObject());
        testIfContainsFieldsForNewUserObjecs(newCompanyUserObject);
        testIfUserDoesNotHavePasswordDefined(newCompanyUserObject);

        expect(isEmptyValue(newCompanyUserObject.p_registry)).toBeFalsy();
        expect(isEmptyValue(newCompanyUserObject.p_isvisitor)).toBeFalsy();
        expect(isEmptyValue(newCompanyUserObject.p_istemp)).toBeFalsy();
        expect(isEmptyValue(newCompanyUserObject.p_active)).toBeFalsy();
        expect(newCompanyUserObject.p_active).toBe(1);
    });
    test('[getNewHostObject] GIVEN the need for a new Company User Object THEN should generate non empty new user object with all fields of a new HOST USER', () => { 
        const newHost = store.dispatch(getNewHostObject({}));
        testIfContainsFieldsForNewUserObjecs(newHost);
        testIfUserDoesHavePasswordDefined(newHost);
    });
    test('[getNewCompanyObject] GIVEN the need for a new Company Object THEN should generate non empty new user object with all fields of a new COMPANY', () => { 
        const newCompany = store.dispatch(getNewCompanyObject({}));
        expect(newCompany.p_cpsw).not.toBeUndefined();
        expect(newCompany.p_cactive).toBe(0);
        expect(newCompany.p_cport).toBe(0);
    });

});


describe("TESTING assignPrivilegesAccordingToUserType: ", () => { 
    test('GIVEN a visitor object THEN show return visitor privileges', () => { 
        const privs = [CREATE_REQUEST_PRIV];
        const userObject = {
            "p_id": 4718,
            "p_fname": "James",
            "p_lname": "Bond1",
            "p_nid": "jamesbond1ID",
            "p_psw": "7KKL6G1G",
            "p_id_provenance": null,
            "p_email": null,
            "p_pnumber_1": "8400000071",
            "p_pnumber_2": "8200000071",
            "p_nationality": "Malaysia",
            "p_id_type_id": 143,
            "p_expiration_date_id": "2020-11-30T00:00:00.000Z",
            "entity": null,
            "p_istemp": 1,
            "p_active": 1,
            "p_isvisitor": 1,
            "p_registry": "2020-11-21T15:33:30.842Z",
            "p_tokken": "Jo0Kq4K6TU9gdsrznajLAGtDX5uPuBNc",
            "p_id_entity": null,
            "p_isblocked": false
        }

        assignPrivilegesAccordingToUserType(userObject);
        userObject.privileges.forEach(priv => {
            expect(privs.includes(priv)).toBeTruthy();
        });
    });
    test('GIVEN a service provicer object THEN show return visitor privileges', () => { 
        const privs = [CREATE_REQUEST_PRIV, ADD_VISITOR_PRIV];
        const userObject = {
            "p_id": 26783,
            "p_fname": "CompuserA",
            "p_lname": "Compuser",
            "p_nid": "93820480234",
            "p_psw": "123456",
            "p_id_provenance": null,
            "p_email": "compusera@gmail.com",
            "p_pnumber_1": "+2583241534143",
            "p_pnumber_2": "+25815143131413",
            "p_nationality": "Mozambique",
            "p_id_type_id": 47,
            "p_expiration_date_id": "2021-06-04T00:00:00.000Z",
            "entity": "DunderMifflin",
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 1,
            "p_registry": "2021-05-20T19:22:48.449Z",
            "p_tokken": "IxQlyFKdvNEfLgrGgSJkafNDwfm3eTpN",
            "p_id_entity": 8181,
            "p_isblocked": null,
            "is_rep": true
        }

        assignPrivilegesAccordingToUserType(userObject);
        userObject.privileges.forEach(priv => {
            expect(privs.includes(priv)).toBeTruthy();
        });
    });
    test('GIVEN a host object THEN show return visitor privileges', () => { 
        const privs = [CREATE_REQUEST_PRIV, CREATE_FAST_REQ_PRIV, CREATE_SERV_REQ_PRIV];
        const userObject = {
            "p_id": 15,
            "p_fname": "Leila",
            "p_lname": "Damons",
            "p_nid": "OLDCHEVY",
            "p_psw": "123456",
            "p_email": "Leila@gmail.com",
            "p_pnumber_1": "847931877",
            "p_pnumber_2": "847931872",
            "p_nationality": "Mozambique",
            "p_id_type_id": 143,
            "p_expiration_date_id": "2022-02-02T00:00:00.000Z",
            "company": "MPDC",
            "department": "IT",
            "p_id_role": 51,
            "p_id_department": 4,
            "p_id_company": 4,
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 0,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQ11"
        }

        assignPrivilegesAccordingToUserType(userObject);
        userObject.privileges.forEach(priv => {
            expect(privs.includes(priv)).toBeTruthy();
        });
    });
    test('GIVEN a thread object THEN show return visitor privileges', () => { 
        const privs = [CREATE_REQUEST_PRIV, CREATE_FAST_REQ_PRIV, MAIN_PANEL_PRIV, GRANT_ACCESS_PRIV, CREATE_SERV_REQ_PRIV];
        const userObject = {
            "p_id": 8,
            "p_fname": "Jeronimo",
            "p_lname": "Tameles",
            "p_nid": "ABCDZ1F",
            "p_psw": "123456",
            "p_email": "Jeronim@gmail.com",
            "p_pnumber_1": "847931874",
            "p_pnumber_2": "847931872",
            "p_nationality": "Mozambique",
            "p_id_type_id": 49,
            "p_expiration_date_id": "2021-04-02T00:00:00.000Z",
            "company": "MPDC",
            "department": "Segurança",
            "p_id_role": 52,
            "p_id_department": 9,
            "p_id_company": 4,
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 0,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQh0"
        }

        assignPrivilegesAccordingToUserType(userObject);
        userObject.privileges.forEach(priv => {
            expect(privs.includes(priv)).toBeTruthy();
        });
    });
    test('GIVEN a coreSecurity object THEN show return visitor privileges', () => { 
        const privs = [CREATE_REQUEST_PRIV, CREATE_FAST_REQ_PRIV, MAIN_PANEL_PRIV, GRANT_ACCESS_PRIV, BLOCK_USER_PRIV, TERMINAL_PANEL_PRIV, SETTINGS_PRIV, REPORTS_PRIV, CREATE_SERV_REQ_PRIV]
        const userObject = {
            "p_id": 9,
            "p_fname": "Isac",
            "p_lname": "Massale",
            "p_nid": "CASRR4FXFF",
            "p_psw": "123456",
            "p_email": "Isac@gmail.com",
            "p_pnumber_1": "847931875",
            "p_pnumber_2": "847931874",
            "p_nationality": "Mozambique",
            "p_id_type_id": 50,
            "p_expiration_date_id": "2021-12-12T00:00:00.000Z",
            "company": "MPDC",
            "department": "Segurança",
            "p_id_role": 53,
            "p_id_department": 9,
            "p_id_company": 4,
            "p_istemp": 0,
            "p_active": 1,
            "p_isvisitor": 0,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQ10"
        }

        assignPrivilegesAccordingToUserType(userObject);
        userObject.privileges.forEach(priv => {
            expect(privs.includes(priv)).toBeTruthy();
        });
    });
    test('GIVEN an unknown object THEN show return false', () => { 
        const userObject = {
            "p_id": 9,
            "p_fname": "Isac",
            "p_lname": "Massale",
            "p_nid": "CASRR4FXFF",
            "p_psw": "123456",
            "p_email": "Isac@gmail.com",
            "p_pnumber_1": "847931875",
            "p_pnumber_2": "847931874",
            "p_nationality": "Mozambique",
            "p_id_type_id": 50,
            "p_expiration_date_id": "2021-12-12T00:00:00.000Z",
            "company": "MPDC",
            "department": "Segurança",
            "p_id_department": 9,
            "p_id_company": 4,
            "p_active": 1,
            "p_registry": "2020-01-02T20:20:20.000Z",
            "p_tokken": "h61JfXxkSA5Nv3uPOSders89iD0iXQ10"
        }

        expect( () => assignPrivilegesAccordingToUserType(userObject)).toThrow();

    });
});
