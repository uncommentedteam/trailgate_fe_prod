# 07/12/2021
[X] - Disable Notification Functionality
[ ] - Searching for requests does [???]
[ ] - Main panel filtering by terminal does not filter by terminals
[ ] - Visitors that made collective visits cannot see their guests

### Things that are missing: 31/12/2021
[ ] - Disable Notification Functionality
[ ] - Searching for requests does [???]
[ ] - Main panel filtering by terminal does not filter by terminals
[ ] - Visitors that made collective visits cannot see their guests

### BUGS 18/12/2021
[ ] - Search nas minhas requisições nao esta funcionar
[x] - When I go to the next page it shows me a msg saying sem visitantes dentro, no visitors inside do dashboard

### BUGS 12/12/2021
- [ ] Para visitas com longos períodos, so podem passar para expirados  se : 
    tiver iniciando e o seu período ja nao tiver terminado; ( we need a call in here. You can check the notes)
- [x] Add a pop out for confirmation on actions except refresh button
- [x] Change de dashboard layout
- [x] Dashboard Filtering is not working

### NOTES
1. I don't know what happened, but this procedure is not being used in the BE "proc_set_visit_started ".You could use this procedure on the first visitor entrance so it would be easier to check visit states (started, in progress, done). This way, it would be easier to check if visit is expired or not. PS: the procedure "proc_set_visit_done" is being used in the method "setDone"
2. Since subterminals now accept request, plase check on prod DB if the id of 'Grindroad' terminal is the same as the id of 'Grindroad Mocambique Lda'


### BUGS 07/12/2021
- [x] Click New Request then Edit request
- [x] Deve ver requisicoes que ainda estao a decorrer por muito tempo
- [x] Disponibilizar as subTerminais para a seleção na Requisição
- [ ] Formatar os numeros provenientes do ficheiro excel para que nao tenham espacos
- [x] Adicionar a instrução de ao ficheiro exel para adicionar um unico email

- [ ] Elaborate Visitor User Guide


### BUGS 05/12/2021
- [x] Change "Nome" to "Nome Material" in Add materials section and "qtd" to "Quantidade"
- [x] Improve the UI for btn notify (Make it look like the refresh button)
- [x] Visit expiration should only be by date
- [x] Add a note to the Excel Document to tell that: 
    - [x] The one making the requisition cannot be added
    - [x] The fields should be unique
    - [x] What are the mandatory fields
- [x] Make sure that the rows are protected from deletetion
- [ ] Preformat excel document in the numbers part,
- [ ] Dashboard Filtering is not working
- [ ] Elaborate Visitor User Guide
- [ ] Change de dashboard layout


### BUGS 04/12/2021
- [x] If  No recipients defined don’t send mailMessage
- [x] Nas mensagens sempre incluir o URL do port gateway\
- [x] If user cannot be associated to visit because of invalid data don’t create the request 
- [x] Button to download  user guide

### BUGS 29/10/2021
[ ] - SMS de rejeicao nao possui motivo de rejeicao
[ ] - Email of company registration sends link with null
[ ] - Alert Messages modal does not have a good formated title


### BUGS 25/10/2021
[ ] - Visits p_date_time inside time input field is wrong
[x] - When fast request fails, it add country code more then one time
[x] - idDocuments not being uploaded
[x] - terms_and_conditions invalid tag on visitorFastVisit form
[x] - participants, license on viewRequisition modal
[x] - Title of terms and condition modal
[x] - Fast Request Success message should tell user about the next steps ("Ira receber um email ou mensagem de texto com a confirmacao da visita")
[x] - Make refresh button more visible and interactive
[x] - Edit user does not update user data
[x] - edit user loses id document number
[x] - Official de Segurança nao pode pedir inducao
[x] - Visitor canmot edit visits when host edits hours
[x] - Creating Host Requests Does not Upload Id Document Image
[x] - When host edits visits the hours reset to 08:00
[x] - Company Registration needs a message
[x] - Terms and condition on Company Regsitration (Title of modal)
[x] - Registration of Company Users Should have the same passwords as default password for registration
[x] - Creating users for Service Provider Account

### BUGS 23/10/2021
[x] - the link on #home button on the navbar should open a new tab to https://www.portmaputo.com/
[x] - the link on #Informacoes Do Porto button on the navbar should open a new tab to https://www.portmaputo.com/category/port-news/
[x] - the link on #Vistias button on the navbar should take to https://gateway.portmaputo.com/ on the same tab
[x] - the link on #Contacte-nos button on the navbar should take to https://gateway.portmaputo.com/contactUs#nowhere on the same taby
[x] - Mixed Content: The page at 'https://gateway.portmaputo.com/' was loaded over HTTPS, but requested an insecure script 'http://code.jquery.com/jquery-1.11.0.min.js'. This request has been blocked; the content must be served over HTTPS.
[x] - gateway.portmaputo.com/:1 Mixed Content: The page at 'https://gateway.portmaputo.com/' was loaded over HTTPS, but requested an insecure script 'http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'. This request has been blocked; the content must be served over HTTPS.
[x] - You need to add https to http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js and http://code.jquery.com/jquery-1.11.0.min.js'
[x] - Public Service visitors should not be inducted.




### BUGS 03/10/2021
# Settings 
- [x] Depois de adicionar um novo extra, o pop out não fecha
- [x] Depois de adicionar um novo extra, duplica alguns na lista ja existente (UI only) então tem que-se mexer no refresh para tudo ficar normal
- [x]  Sempre que edito algo nas definições tenho de clicar o refresh (not a priority)
- [x] Quando tento adicionar uma terminal, no numero de telemóvel com símbolos, nao permite
- [x] Quando Salvo ou edit qualquer coisa nas definições, a mensagem de sucesso vem sem msg
- [x] Quando quero editar serviços públicos nao aprece o check box de serviços publicos
- [x] Lista de Terminal duplicando serviços publicos
### FIXES 15/10/2021
- [x] Depois de-se criar user o psw default para todos deve ser 12345678 (porque por vezes twillow nao manda sms com credencias principalmente quando é uma lista varias pessoas)
- [x] Todas contas criadas ja vem activas nao precisam receber link nenhum para activar ate mesmo contas do Public service e com psw default 12345678
- [x] Falha no processamento de dados continua a registrar o usuario
- [x] Quando crio requisição com material quando submeto o material ou matricula nao aparece
- [x] Cancelar e depois aceitar deve ser permitido
- [x] Change name from Tailgate to Port Gateway
- [x] password recovery not working
- [x] password recovery for phone numbers too
- [x] Login page background is on repeat
- [x] Temos de implementar username
- [x] Extras are duplicated on input fields
- [ ] Oficias de segurança da MPDC (SO MPDC) pode mandar fazer indução e aprovar essas induções 
- [ ] request methods should read errors from server.
### PACKAGES DOCUMENTATION
>> Suneditor: https://openbase.com/js/suneditor-react/documentation#basic-settings
              https://github.com/JiHong88/SunEditor/blob/master/README.md#install